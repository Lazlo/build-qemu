#!/bin/bash

set -e
set -u

d="qemu"
if [ -d $d ]; then
	cd $d
	git pull origin master
	git submodule update --recursive
	exit 0
fi
git clone --depth 1 --recursive https://git.qemu.org/git/qemu.git
