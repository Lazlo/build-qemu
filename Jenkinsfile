@Library('pipeline-library') _

pipeline {
    agent any
    environment {
        APT_LOCK    = "apt-lock-${env.NODE_NAME}"
        SRC_DIR        = "qemu"
        DESTDIR        = "fakeroot"
        ARCHIVE        = "qemu.tar.gz"
    }
    stages {
        stage('Setup') {
            steps {
                lock("${env.APT_LOCK}") {
                    echo 'Setting up ...'
                    sh 'sudo ./setup.sh'
                }
            }
        }
        stage('Fetch') {
            steps {
                echo 'Fetching ...'
                sh './fetch.sh'
            }
        }
        stage('Clean') {
            steps {
                echo 'Cleaning ...'
                dir("${env.SRC_DIR}") {
                    sh 'git clean -dfx'
                }
                sh "rm -rf ${env.DESTDIR}"
            }
        }
        stage('Build') {
            steps {
                echo 'Building ...'
                sh './build.sh'
            }
        }
        stage('Package') {
            steps {
                echo 'Packaging ...'
                sh "tar czf ${env.ARCHIVE} ${env.DESTDIR}"
                archiveArtifacts artifacts: "${env.ARCHIVE}"
            }
        }
    }
    post {
        always {
            commonStepNotification()
        }
    }
}
