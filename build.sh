#!/bin/bash

set -e
set -u

default_destdir="$PWD/fakeroot/usr/local"
default_configure_args=""
default_configure_args="$default_configure_args --prefix=$default_destdir"
default_configure_args="$default_configure_args --cc=/usr/lib/ccache/gcc"
default_configure_args="$default_configure_args --cxx=/usr/lib/ccache/g++"
default_make="make"
default_makeflags="-j$(nproc)"

CONFIGURE_ARGS="${CONFIGURE_ARGS:-$default_configure_args}"
MAKE="${MAKE:-$default_make}"
MAKEFLAGS="${MAKEFLAGS:-$default_makeflags}"

mkdir -p $default_destdir
cd qemu
./configure $CONFIGURE_ARGS
$MAKE $MAKEFLAGS
$MAKE $MAKEFLAGS install
