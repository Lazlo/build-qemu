#!/bin/bash

set -e
set -u

pkgs=""
pkgs="$pkgs make gcc ccache"
pkgs="$pkgs libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev"
pkgs="$pkgs libsdl1.2-dev"

apt-get -q install -y $pkgs
